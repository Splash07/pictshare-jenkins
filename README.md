# Triển khai multi-process process container cho pictshare.

Reopo tham khảo: https://github.com/richarvey/nginx-php-fpm

## Cách làm:
* Sử dụng docker image base: https://hub.docker.com/r/bitnami/php-fpm/, tag 7.3
* Viết Dockerfile pictshare tương tự ở repo tham khảo với yêu cầu như sau:
    * Update config của nginx, php7-fpm, supervisor vào thư mục config
    * Cài package nginx, supervisor
    * Tạo user nginx.
    * Update config php7-fpm sử dụng chạy bằng user nginx
    * Viết entrypoint.sh: update domain pictshare trong config nếu có env DOMAIN được truyền vào, sau đó start supervisor



## BUILD:
docker build -t test .

## RUN
docker run -it --name test -e DOMAIN=abc.com -d test

## Check IP
docker inspect test

## map host client /etc/hosts
<ip> <domain>
