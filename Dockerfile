FROM bitnami/php-fpm:7.3 
#TODO
# check version: apt-cache policy <package name>
ENV NGINX_VERSION 1.14.2-2+deb10u1 
ENV SUPERVISOR_VERSION 3.3.5-1 

RUN apt-get update
#RUN apt-get install -y nginx=$NGINX_VERSION supervisor=$SUPERVISOR_VERSION
RUN apt-get install -y nginx supervisor
RUN useradd nginx
RUN usermod -s /usr/sbin/nologin nginx
# copy code and config

COPY pictshare /var/www/html
RUN chown -R nginx:nginx /var/www/html
COPY config/config.inc.php /var/www/html/inc/
COPY config/pictshare-nginx.conf /etc/nginx/conf.d/
COPY config/www.conf /etc/php-fpm.d/
COPY config/supervisord.conf /etc/

RUN mkdir -p /var/log/php-fpm
RUN touch /var/log/php-fpm/www-slow.log
RUN chown nginx:nginx /var/log/php-fpm/www-slow.log

COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
