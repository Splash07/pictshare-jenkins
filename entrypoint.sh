#!/bin/bash

export DEFAULT_DOMAIN=datnt.pictshare.com

echo "DOMAIN=  ${DOMAIN}"
#########################
# daonv: replace if have env DOMAIN
if [ ! -z ${DOMAIN} ]; then 
    echo "DOMAIN= $DOMAIN"
    sed -i "s/$DEFAULT_DOMAIN/$DOMAIN/g" /etc/nginx/conf.d/pictshare-nginx.conf
    sed -i "s/$DEFAULT_DOMAIN/$DOMAIN/g" /var/www/html/inc/config.inc.php
fi
exec /usr/bin/supervisord -n -c /etc/supervisord.conf
